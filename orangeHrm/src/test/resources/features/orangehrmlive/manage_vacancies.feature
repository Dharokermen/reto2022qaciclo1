Feature: Manage vacancies
  As the administrator
  I want to be able to manage the vacancies at Orange Web Page
  To have better control of the recruitment process in the company

  Background:
    Given I enter to the login Page of Orange Web Page
    When  I login and enter to the vacancies section

  Scenario: Add a new vacancy
    When   I add a new vacancy job information and save it
    Then  The job Vacancy should appear in the Vacancies section

  Scenario: Miss a field when adding new vacancy
    When   I add new vacancy job information without Hiring Manager Name and try to save it
    Then  I should not be able to save vacancy