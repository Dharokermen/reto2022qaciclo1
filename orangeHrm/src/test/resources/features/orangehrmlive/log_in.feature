Feature: log in

  As an administrator
  I want to be able to log in in Orange Web Page
  So that I can see my account

  Background:
    Given I enter to the login Page of Orange Web Page

  Scenario: Successful log in
    When  I enter my user information
    Then  I should see my information account

  Scenario: Unsuccessful log in
    When I only enter my user name
    Then I should see an error account validation