# new feature
# Tags: optional

Feature: log out
  As an administrator
  I want to be able to logout from orange page
  To end my session


  Scenario: log out session
    Given I login to the orange home page
    When  I end my work and log out
    Then  i should see the login page