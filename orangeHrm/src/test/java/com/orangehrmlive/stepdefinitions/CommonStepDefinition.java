package com.orangehrmlive.stepdefinitions;

import com.orangehrmlive.stepdefinitions.setup.SetUp;
import io.cucumber.java.en.Given;
import org.apache.log4j.Logger;

import static com.orangehrmlive.task.landingpage.OpenLandingPage.openLandingPage;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class CommonStepDefinition extends SetUp {
    private static final Logger LOGGER = Logger.getLogger(ManageVacanciesTestStepDefinition.class);
    private static final String ACTOR_NAME = "Admin";

    @Given("I enter to the login Page of Orange Web Page")
    public void iEnterToTheLoginPageOfOrangeWebPage() {

        try{
            actorSetupTheBrowser(ACTOR_NAME);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage()
            );
        }catch (Exception exception){
            LOGGER.error(exception);

        }
    }
}