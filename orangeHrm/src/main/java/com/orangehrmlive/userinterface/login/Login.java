package com.orangehrmlive.userinterface.login;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Login extends PageObject {

    public static final Target USER_NAME = Target
            .the("Username")
            .located(By.id("txtUsername"));

    public static final Target PASSWORD = Target
            .the("Password")
            .located(By.id("txtPassword"));

    public static final Target LOGIN_BTN = Target
            .the("Login")
            .located(By.id("btnLogin"));

    public static final Target WELCOME_LOGIN = Target
            .the("Welcome Message")
            .located(By.id("welcome"));

    public static final Target SPAN_MESSAGE = Target
            .the("span message")
            .located(By.id("spanMessage"));

}
