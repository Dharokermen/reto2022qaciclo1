package com.orangehrmlive.task.fill;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.conditions.Check;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.orangehrmlive.userinterface.qualificationpage.QualificationPage.*;
import static com.orangehrmlive.util.EnumTimeOut.TWENTY_TWO;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class FillQualificationsSkillAndLanguage implements Task {

    private String skill;
    private String yearsOfExperience;
    private String skillComment;

    private String language;
    private String languageFluency;
    private String languageCompetency;
    private String languageComment;

    public FillQualificationsSkillAndLanguage selectingTheSkill(String skill) {
        this.skill = skill;
        return this;
    }

    public FillQualificationsSkillAndLanguage typingTheYearsOfExperience(String yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
        return this;
    }

    public FillQualificationsSkillAndLanguage typingTheSkillComment(String skillComment) {
        this.skillComment = skillComment;
        return this;
    }

    public FillQualificationsSkillAndLanguage selectingTheLanguage(String language) {
        this.language = language;
        return this;
    }

    public FillQualificationsSkillAndLanguage selectingTheLanguageFluency(String languageFluency) {
        this.languageFluency = languageFluency;
        return this;
    }

    public FillQualificationsSkillAndLanguage selectingTheLanguageCompetency(String languageCompetency) {
        this.languageCompetency = languageCompetency;
        return this;
    }

    public FillQualificationsSkillAndLanguage typingTheLanguageComment(String languageComment) {
        this.languageComment = languageComment;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                WaitUntil.the   (ADD_SKILL_BTN,isVisible()).forNoMoreThan(TWENTY_TWO.getValue()).seconds(),
                Check.whether(DELETE_SKILL_BTN.resolveFor(actor).isVisible())
                        .andIfSo(
                                Scroll.to(SELECT_ALL_SKILLS),
                                Click.on(SELECT_ALL_SKILLS),

                                Scroll.to(DELETE_SKILL_BTN),
                                Click.on(DELETE_SKILL_BTN)
                        ),

                Scroll.to(ADD_SKILL_BTN),
                Click.on(ADD_SKILL_BTN),

                Scroll.to(SELECT_SKILL),
                SelectFromOptions.byValue(skill).from(SELECT_SKILL),

                Scroll.to(YEARS_OF_EXPERIENCE),
                Enter.theValue  (yearsOfExperience).into(YEARS_OF_EXPERIENCE),

                Scroll.to(SKILL_COMMENTS),
                Enter.theValue  (skillComment).into(SKILL_COMMENTS),

                Scroll.to(SKILL_SAVE_BTN),
                Click.on        (SKILL_SAVE_BTN),

                Check.whether(DELETE_LANGUAGE_BTN.resolveFor(actor).isVisible())
                        .andIfSo(
                                Scroll.to(SELECT_ALL_LANGUAGES),
                                Click.on(SELECT_ALL_LANGUAGES),

                                Scroll.to(DELETE_LANGUAGE_BTN),
                                Click.on(DELETE_LANGUAGE_BTN)
                        ),

                Scroll.to(ADD_LANGUAGE_BTN),
                Click.on(ADD_LANGUAGE_BTN),

                Scroll.to(SELECT_LANGUAGE),
                SelectFromOptions.byValue(language).from(SELECT_LANGUAGE),

                Scroll.to(SELECT_FLUENCY),
                SelectFromOptions.byValue(languageFluency).from(SELECT_FLUENCY),

                Scroll.to(SELECT_COMPETENCY),
                SelectFromOptions.byValue(languageCompetency).from(SELECT_COMPETENCY),

                Scroll.to(LANGUAGE_COMMENTS),
                Enter.theValue  (languageComment).into(LANGUAGE_COMMENTS),

                Scroll.to(LANGUAGE_SAVE_BTN),
                Click.on        (LANGUAGE_SAVE_BTN)

        );
    }

    public static FillQualificationsSkillAndLanguage fillQualificationsSkillAndLanguage(){
        return new FillQualificationsSkillAndLanguage();
    }
}
