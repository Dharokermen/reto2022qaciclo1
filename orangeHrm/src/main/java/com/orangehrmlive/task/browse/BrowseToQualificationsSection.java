package com.orangehrmlive.task.browse;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.orangehrmlive.userinterface.qualificationpage.QualificationPage.*;
import static com.orangehrmlive.util.EnumTimeOut.TWENTY_TWO;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class BrowseToQualificationsSection implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the           (MY_INFO_MENU,isVisible()).forNoMoreThan(TWENTY_TWO.getValue()).seconds(),

                Scroll.to               (MY_INFO_MENU),
                Click.on                (MY_INFO_MENU),

                Scroll.to               (QUALIFICATIONS),
                Click.on                (QUALIFICATIONS)
        );
    }

    public static BrowseToQualificationsSection browseToQualificationsSection(){
        return new BrowseToQualificationsSection();
    }
}
