package com.orangehrmlive.task.browse;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.orangehrmlive.userinterface.vacancypage.VacancyPage.*;
import static com.orangehrmlive.util.EnumTimeOut.TWENTY_TWO;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class BrowseBackToVacancySection implements Task {

    private String vacancyName;

    public BrowseBackToVacancySection andSelectingTheVacancyName(String vacancyName) {
        this.vacancyName = vacancyName;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                WaitUntil.the   (VACANCY_BACK_BTN,isVisible()).forNoMoreThan(TWENTY_TWO.getValue()).seconds(),

                Scroll.to       (VACANCY_BACK_BTN),
                Click.on        (VACANCY_BACK_BTN),

                WaitUntil.the   (VACANCY_SEARCH,isVisible()).forNoMoreThan(TWENTY_TWO.getValue()).seconds(),

                Scroll.to       (VACANCY_SEARCH),
                SelectFromOptions.byVisibleText(vacancyName).from(VACANCY_SEARCH),

                Scroll.to       (VACANCY_SEARCH_BTN),
                Click.on        (VACANCY_SEARCH_BTN)
        );

    }

    public static BrowseBackToVacancySection browseBackToVacancySection(){
        return new BrowseBackToVacancySection();
    }
}
