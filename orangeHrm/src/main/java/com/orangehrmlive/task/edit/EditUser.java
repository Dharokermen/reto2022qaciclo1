package com.orangehrmlive.task.edit;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.orangehrmlive.userinterface.myinfointerface.MyInfoInterface.*;
import static com.orangehrmlive.util.EnumTimeOut.TWENTY_TWO;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class EditUser implements Task {
    private String firstName;

    public EditUser useFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the           (BTN,isVisible()).forNoMoreThan(TWENTY_TWO.getValue()).seconds(),

                Scroll.to               (BTN),
                Click.on                (BTN),

                Scroll.to               (FIRST_NAME_DETAILS),
                Enter.theValue(firstName).into   (FIRST_NAME_DETAILS),

                Scroll.to               (BTN),
                Click.on                (BTN)

        );

    }

    public static EditUser editUser(){
        return new EditUser();
    }
}
