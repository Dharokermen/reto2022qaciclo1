package com.orangehrmlive.question.fill;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.orangehrmlive.userinterface.qualificationpage.QualificationPage.SKILL_YEARS_OF_EXPERIENCE_VALIDATION;

public class SkillInformationAdded implements Question<Boolean> {

    private String skillValidationMessage;

    public SkillInformationAdded wasValidatedWithYearsOfExperience(String skillValidationMessage) {
        this.skillValidationMessage = skillValidationMessage;
        return this;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return SKILL_YEARS_OF_EXPERIENCE_VALIDATION.resolveFor(actor).containsText(skillValidationMessage);
    }

    public SkillInformationAdded is(){
        return this;
    }

    public static SkillInformationAdded skillInformationAdded(){
        return new SkillInformationAdded();
    }

}
