package com.orangehrmlive.question.fill;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.orangehrmlive.userinterface.qualificationpage.QualificationPage.WORK_EXPERIENCE_VALIDATION;

public class WorkExperienceAdded implements Question<Boolean> {

    private String workExperienceValidationMessage;

    public WorkExperienceAdded wasValidatedWithCompanyName(String workExperienceValidationMessage) {
        this.workExperienceValidationMessage = workExperienceValidationMessage;
        return this;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return WORK_EXPERIENCE_VALIDATION.resolveFor(actor).containsText(workExperienceValidationMessage);
    }

    public WorkExperienceAdded is(){
        return this;
    }

    public static WorkExperienceAdded workExperienceAdded(){
        return new WorkExperienceAdded();
    }
}
