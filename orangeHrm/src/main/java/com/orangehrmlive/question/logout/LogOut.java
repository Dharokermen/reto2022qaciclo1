package com.orangehrmlive.question.logout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.orangehrmlive.userinterface.logout.Logout.MSG_OK_LOGOUT;
import static com.orangehrmlive.util.Dictionary.LOG_OUT_RESPONSE;

public class LogOut implements Question<Boolean> {

    public LogOut is(){
        return this;
    }
    @Override
    public Boolean answeredBy(Actor actor) {
        return (
                MSG_OK_LOGOUT.resolveFor(actor).containsOnlyText(LOG_OUT_RESPONSE)
                );
    }

    public static LogOut logOut(){
        return new LogOut();
    }
}
