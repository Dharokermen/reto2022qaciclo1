package com.orangehrmlive.question.login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.orangehrmlive.userinterface.login.Login.WELCOME_LOGIN;

public class UserLoggedIn implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {
        return WELCOME_LOGIN.resolveFor(actor).isVisible();
    }

    public UserLoggedIn is(){
        return this;
    }

    public static UserLoggedIn userLoggedInValidatedWithVisibleWelcomeMessage(){
        return new UserLoggedIn();
    }

}
