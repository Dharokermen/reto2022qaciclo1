package com.orangehrmlive.question.login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.orangehrmlive.userinterface.login.Login.SPAN_MESSAGE;

public class UserNotLoggedIn implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {
        return SPAN_MESSAGE.resolveFor(actor).isVisible();
    }

    public UserNotLoggedIn is(){
        return this;
    }

    public static UserNotLoggedIn userNotLoggedInValidatedWithVisibleMessage(){
        return new UserNotLoggedIn();
    }

}
