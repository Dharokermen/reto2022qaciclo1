package webservices.org.oorsprong.util;

import static com.google.common.base.StandardSystemProperty.USER_DIR;

public class Dictionary {

    private Dictionary() {
    }

    public static final String SOAP_ACTION = "SOAPAction";
    public static final String COUNTRY_NOT_FOUND_IN_DATABASE_MESSAGE = "Country not found in the database";


    public static final String COUNTRY_BY_NAME_WRONG_XML = USER_DIR.value() + "\\src\\test\\resources\\files\\soap\\CountryByNameFail.xml";
    public static final String URL_BASE = "http://webservices.oorsprong.org";
    public static final String RESOURCE = "/websamples.countryinfo/CountryInfoService.wso";


    public static final String FLAG_XML_FILE_PATH = "src/test/resources/files/soap/flagCode.xml";
    public static final String LINUX_FOLDER_SEPARATOR = "/";
    public static final String COUNTRY_FLAG_ISO_CODE_XML_LINUX = USER_DIR.value() + LINUX_FOLDER_SEPARATOR + FLAG_XML_FILE_PATH;
    public static final String FLAG_FILE_AN_RETURN = "http://www.oorsprong.org/WebSamples.CountryInfo/Flags/Netherlands_Antilles.jpg";

    public static final String NAME_COUNTRY_ISO_CODE_XML = USER_DIR.value() + "/src/test/resources/files/soap/countryISOName.xml";

    public static final String LIST_COUNTRY_BY_NAME_XML = USER_DIR.value() + "/src/test/resources/files/soap/CountryByName.xml";

    public static final String COUNTRY_NAME_XML_FILE_PATH = "src/test/resources/files/soap/countryName.xml";
    public static final String COUNTRY_NAME_XML_LINUX_PATH = USER_DIR.value() + LINUX_FOLDER_SEPARATOR + COUNTRY_NAME_XML_FILE_PATH;
}
