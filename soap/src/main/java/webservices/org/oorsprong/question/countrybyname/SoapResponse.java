package webservices.org.oorsprong.question.countrybyname;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.rest.questions.LastResponse;

import java.nio.charset.StandardCharsets;

public class SoapResponse implements Question {

    @Override
    public String  answeredBy(Actor actor) {
        return new String(LastResponse.received().answeredBy(actor).asByteArray(), StandardCharsets.UTF_8);
    }

    public static SoapResponse response(){
        return new SoapResponse();
    }
}
