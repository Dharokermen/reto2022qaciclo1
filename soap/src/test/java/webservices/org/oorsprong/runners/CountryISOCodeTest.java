package webservices.org.oorsprong.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        glue = {"webservices.org.oorsprong.stepdefinition.countryisocode"},
        features = {"src/test/resources/features/soap/countryISO.feature"},
        tags = ""
)
public class CountryISOCodeTest {
}
