package webservices.org.oorsprong.runners;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberSerenityRunner;
import org.junit.runner.RunWith;

@RunWith(CucumberSerenityRunner.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/soap/countryName.feature"},
        glue = {"webservices.org.oorsprong.stepdefinition.convertcountryname"}

        //prueba jenkins 31
)
public class ConvertCountryNameTest {
}
