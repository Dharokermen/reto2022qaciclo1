package webservices.org.oorsprong.stepdefinition.listofcountrybyname;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import webservices.org.oorsprong.runners.ListOfCountriesByNameTest;
import webservices.org.oorsprong.stepdefinition.SetupLog4j2;

import java.util.HashMap;
import java.util.Map;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static webservices.org.oorsprong.task.countrybyname.DoPost.doPost;
import static webservices.org.oorsprong.util.Dictionary.*;
import static webservices.org.oorsprong.util.Utilities.readFile;

public class ListOfCountriesByNameStepDefinition extends SetupLog4j2 {

    private final Map<String, Object> headers = new HashMap<>();
    private static final String SOAP_ACTION = "SOAPAction";
    private final Actor actor = Actor.named("Bernabe");
    private String bodyRequest;
    private static final Logger LOGGER = Logger.getLogger(ListOfCountriesByNameTest.class);

    @Given("I am in the web site")
    public void i_am_in_the_web_site() {
        setup();
        actor.can(CallAnApi.at(URL_BASE));
        headers.put("Content-Type", "text/xml;charset=UTF-8");
        headers.put(SOAP_ACTION, "");
        bodyRequest = defineBodyRequest(LIST_COUNTRY_BY_NAME_XML);
        LOGGER.info(bodyRequest);


    }

    @When("I request the list of countries")
    public void i_request_the_list_of_countries() {
        actor.attemptsTo(
                doPost()
                        .usingTheResource(RESOURCE)
                        .withHeaders(headers)
                        .andBodyRequest(bodyRequest)
        );

    }

    @Then("i should all the countries of the web site")
    public void i_should_all_the_countries_of_the_web_site() {
        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_OK,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)
                )
                );

    }


    private String defineBodyRequest(String File){
        return readFile(File)
                ;
    }


    @Given("i am already on the web site")
    public void i_am_already_on_the_web_site() {
        setup();
        actor.can(CallAnApi.at(URL_BASE));
        headers.put("Content-Type", "text/xml;charset=UTF-8");
        headers.put(SOAP_ACTION, "");
        bodyRequest = defineBodyRequest(COUNTRY_BY_NAME_WRONG_XML);
        LOGGER.info(bodyRequest);

    }

    @When("i request the list of countries but it is not done corectly")
    public void i_request_the_list_of_countries_but_it_is_not_done_corectly() {
        actor.attemptsTo(
                doPost()
                        .usingTheResource(RESOURCE)
                        .withHeaders(headers)
                        .andBodyRequest(bodyRequest)
        );

    }

    @Then("i should receive a fail message")
    public void i_should_receive_a_fail_message() {
        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_INTERNAL_SERVER_ERROR,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                )
        );


    }


}
