package webservices.org.oorsprong.stepdefinition.convertcountryname;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import webservices.org.oorsprong.stepdefinition.SetupLog4j2;

import java.util.HashMap;
import java.util.Map;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.containsString;
import static webservices.org.oorsprong.question.countrybyname.SoapResponse.response;
import static webservices.org.oorsprong.task.countrybyname.DoPost.doPost;
import static webservices.org.oorsprong.util.CountryISOCodeKey.COUNTRY_ISO_CODE;
import static webservices.org.oorsprong.util.Dictionary.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static webservices.org.oorsprong.util.Utilities.readFile;

public class ConvertCountryNameStepDefinition extends SetupLog4j2 {

    private final Map<String, Object> headers = new HashMap<>();
    private final Actor actor = Actor.named("Carlos");
    private String bodyRequest;
    private static final Logger LOGGER = Logger.getLogger(ConvertCountryNameStepDefinition.class);

    @Given("that the user is in the web resource giving the country ISO name {string}")
    public void thatTheUserIsInTheWebResourceGivingTheCountryISOName(String countryIsoCode) {
        setup();
        actor.can(CallAnApi.at(URL_BASE));
        headers.put("Content-Type", "text/xml;charset=UTF-8");
        headers.put(SOAP_ACTION, "");
        bodyRequest = defineBodyRequest(countryIsoCode);
        LOGGER.info(bodyRequest);
    }
    @When("the user generates the query")
    public void theUserGeneratesTheQuery() {
        actor.attemptsTo(
                doPost()
                        .usingTheResource(RESOURCE)
                        .withHeaders(headers)
                        .andBodyRequest(bodyRequest)
        );
    }
    @Then("the user will see the formal name of the country as {string}")
    public void theUserWillSeeTheFormalNameOfTheCountryAs(String formalName) {
        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_OK,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)
                ),
                seeThat("El nombre formal de la moneda debe ser: " + formalName,
                        response(), containsString(formalName))
        );
    }

    @Given("that the user is in the web resource giving the something similar to an ISO name like {string}")
    public void thatTheUserIsInTheWebResourceGivingTheSomethingSimilarToAnISONameLike(String fakeIsoCode) {
        setup();
        actor.can(CallAnApi.at(URL_BASE));
        headers.put("Content-Type", "text/xml;charset=UTF-8");
        headers.put(SOAP_ACTION, "");
        bodyRequest = defineBodyRequest(fakeIsoCode);
        LOGGER.info(bodyRequest);
    }

    @Then("the user will see that there is no country name for that code in the database")
    public void theUserWillSeeThatThereIsNoCountryNameForThatCodeInTheDatabase() {
        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_OK,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)
                ),
                seeThat("El nombre de la moneda no se encontró en la base de datos",
                        response(), containsString(COUNTRY_NOT_FOUND_IN_DATABASE_MESSAGE))
        );
    }

    private String defineBodyRequest(String countryIsoCode){
        return readFile(NAME_COUNTRY_ISO_CODE_XML)
                .replace(COUNTRY_ISO_CODE.getValue(), countryIsoCode);
    }
}
