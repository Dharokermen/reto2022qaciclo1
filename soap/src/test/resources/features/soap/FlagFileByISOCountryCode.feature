  Feature: Country Flag
  As a user
  I need to get a country flag file from a country code
  To use them in the next forms

  Scenario: Get the user flag using a country code
    Given that the user is using the web resource and using the country code "AN"
    When the user generates the required query
    Then he will see the file name with the flag

    Scenario: The user gives a wrong country code
      Given that the user is using the web resource and using the country code "ZU"
      When the user generates the required query
      Then the resource send the error message: "Country not found in the database"