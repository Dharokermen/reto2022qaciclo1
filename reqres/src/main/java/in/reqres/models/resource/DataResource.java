package in.reqres.models.resource;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonPropertyOrder({
        "id",
        "name",
        "year",
        "color",
        "pantone_value"
})
public class DataResource {
    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("id")
    private int id;

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("name")
    private String name;

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("year")
    private String year;

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("color")
    private String color;

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("pantone_value")
    private String pantoneValue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPantoneValue() {
        return pantoneValue;
    }

    public void setPantoneValue(String pantoneValue) {
        this.pantoneValue = pantoneValue;
    }
}