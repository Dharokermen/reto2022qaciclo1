package in.reqres.question.post;

import in.reqres.models.UpdatableUser;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class CreateUserResponse implements Question<UpdatableUser> {
    public static CreateUserResponse createUser(){
        return new CreateUserResponse();
    }

    @Override
    public UpdatableUser answeredBy(Actor actor) {
        return SerenityRest.lastResponse().as(UpdatableUser.class);
    }
}
