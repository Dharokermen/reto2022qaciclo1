package in.reqres.task.delete;


import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Delete;
import java.nio.charset.StandardCharsets;


public class DoDeleteWithParam implements Task {
    private String resource;
    private String param;
    private String value;

    public static DoDeleteWithParam deleteUser() {
        return new DoDeleteWithParam();
    }

    public DoDeleteWithParam usingTheResource(String resource) {
        this.resource = resource;
        return this;
    }

    public DoDeleteWithParam setParamAndValue(String param, String value) {
        this.param = param;
        this.value = value;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Delete.from(resource)
                        .with(requestSpecification ->
                                requestSpecification.pathParam(param, value)
                                        .contentType(ContentType.JSON.withCharset(StandardCharsets.UTF_8))
                        )
        );
    }
}
