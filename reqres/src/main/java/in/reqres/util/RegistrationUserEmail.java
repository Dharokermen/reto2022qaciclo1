package in.reqres.util;

public enum RegistrationUserEmail {
    USER_ONE("george.bluth@reqres.in"),
    USER_TWO("janet.weaver@reqres.in");

    private final String value;

    RegistrationUserEmail(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
