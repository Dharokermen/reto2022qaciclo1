package in.reqres.util;

public enum GetResources implements ReplaceStringRegex {
    VIEW_SINGLE_USER("/api/users/{id}"),
    VIEW_SINGLE_RESOURCE("/api/unknown/{id}");

    private final String value;

    GetResources(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }


    @Override
    public String replaceParam(String param) {
        return replaceString(value,param);
    }
}
