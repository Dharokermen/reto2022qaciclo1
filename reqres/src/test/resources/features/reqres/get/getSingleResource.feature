# new feature
# Tags: optional

Feature: check a page resource
  Me as administrator of the page
  I want to get the characteristics of the resource
  To verify the information on the page

  Background: Administrator on the page
    Given the administrator enter on the page

  Scenario: Check the characteristics of the resource
    When enter the id of the resource
    Then the characteristics of the resource should be displayed

  Scenario: Check the characteristics of the missing resource
    When you enter the id of the nonexistent resource
    Then the characteristics of the resource should not be displayed

