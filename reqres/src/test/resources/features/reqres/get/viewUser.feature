Feature:View user information

  As an administrator of the page
  I want to see information of a user
  To keep track of users

  Background: Administrator on the web page
    Given the administrator enters the page

  Scenario: Successfully view a user information
    When the administrator enters the user id
    Then the administrator views the user information

  Scenario: User not found
    When the administrator enters the id of the nonexistent user
    Then the administrator does not see any information