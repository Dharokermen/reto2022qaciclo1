# new feature
# Tags: optional

Feature: Update user data
  As an user of ReqRes
  I want to be able to update my data
  To keep my personal information up to date

  Scenario: Successfully data updated
    Given the user has access to the appropriate web resource
    When the user want to update his data
    Then the user can see his new personal information