Feature:Register User
  As a new user of the page
  I want to register
  To make transactions

  Background: Access to main page
    Given the client enters on the page

  Scenario: Successful registration
    When creates a new user
    Then you will get an authentication token

  Scenario: Failed registration
    When create a user without password
    Then he will get an error message