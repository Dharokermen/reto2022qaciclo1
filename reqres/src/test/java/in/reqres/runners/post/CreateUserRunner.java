package in.reqres.runners.post;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/reqres/post/createUser.feature"},
        glue = "in.reqres.stepdefinitions.post",
        plugin = {"pretty", "html:target/cucumber-reports"}
)
public class CreateUserRunner {
}
