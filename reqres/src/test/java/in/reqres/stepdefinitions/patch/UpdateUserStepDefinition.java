package in.reqres.stepdefinitions.patch;

import in.reqres.models.UpdatableUser;
import in.reqres.stepdefinitions.put.ModifyUserStepDefinition;
import in.reqres.stepdefinitions.setup.BaseResources;
import in.reqres.util.PatchResources;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;

import static in.reqres.question.CheckHttpResponse.checkHttpResponse;
import static in.reqres.question.patch.UpdateUserResponse.updateUser;
import static in.reqres.task.patch.DoPatchWithParam.doPatchWithParam;
import static in.reqres.util.RandomHelper.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

public class UpdateUserStepDefinition extends BaseResources {

    private UpdatableUser resultingUserData = new UpdatableUser();
    private final UpdatableUser newUserData = new UpdatableUser();

    private static final Logger LOGGER = Logger.getLogger(ModifyUserStepDefinition.class);
    private static final Actor user = new Actor("User");

    @Given("the user has access to the appropriate web resource")
    public void theUserHasAccessToTheAppropriateWebResource() {
        generalSetUp();
        try {
            user.whoCan(CallAnApi.at(BASE_REQRES));
            LOGGER.info("Getting in the URI: ".concat(BASE_REQRES));
        } catch (Exception exception) {
            LOGGER.error("An error occurred while entering the base URI: ".concat(BASE_REQRES));
            Assertions.fail(exception);
        }
    }

    @When("the user want to update his data")
    public void theUserWantToUpdateHisData() {
        newUserData.setName(randomFirstName());
        newUserData.setJob(randomJob());

        String userId = randomUserId();

        try {
            user.attemptsTo(
                    doPatchWithParam()
                            .usingTheResource(PatchResources.UPDATE_USER_WITH_PATCH.getValue())
                            .setParamAndValue("id", userId)
                            .setBodyRequest(newUserData.toJson())

            );
            LOGGER.info("Successful access to PATCH resource: ".concat(PatchResources.UPDATE_USER_WITH_PATCH.replaceParam(userId)));
            LOGGER.info("User data to update:\n".concat(newUserData.toJson()));
        } catch (Exception exception) {
            LOGGER.error("An error occurred while getting PATCH resource: ".concat(PatchResources.UPDATE_USER_WITH_PATCH.replaceParam(userId)));
            Assertions.fail(exception);
        }
    }

    @Then("the user can see his new personal information")
    public void theUserCanSeeHisNewPersonalInformation() {
        checkHttpResponse().setExpectedResponse(HttpStatus.SC_OK).answeredBy(user);

        resultingUserData = updateUser().answeredBy(user);

        user.should(
                seeThat("the name is updated with his new name",
                        validateResponse -> resultingUserData.getName(), equalTo(newUserData.getName())),
                seeThat("the job is updated with his new job",
                        validateResponse -> resultingUserData.getJob(), equalTo(newUserData.getJob())),
                seeThat("has a date",
                        validateResponse -> resultingUserData.getName(), notNullValue())
        );
    }
}
