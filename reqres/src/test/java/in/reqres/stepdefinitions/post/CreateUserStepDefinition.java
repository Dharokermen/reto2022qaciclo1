package in.reqres.stepdefinitions.post;

import in.reqres.models.UpdatableUser;
import in.reqres.stepdefinitions.setup.BaseResources;
import in.reqres.util.PostResources;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;

import static in.reqres.question.CheckHttpResponse.checkHttpResponse;
import static in.reqres.question.post.CreateUserResponse.createUser;
import static in.reqres.task.post.DoPost.doPost;
import static in.reqres.util.RandomHelper.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;


public class CreateUserStepDefinition extends BaseResources {

    private final UpdatableUser userData = new UpdatableUser();
    private UpdatableUser createdData = new UpdatableUser();

    private static final Logger LOGGER = Logger.getLogger(CreateUserStepDefinition.class);
    private static final Actor USER = new Actor("user");

    @Given("the user is on the web page")
    public void the_user_is_on_the_web_page() {
        generalSetUp();
        try {
            USER.whoCan(CallAnApi.at(BASE_REQRES));
            LOGGER.info("Getting in the URI: ".concat(BASE_REQRES));
        }catch (Exception exception) {
            LOGGER.error("An error occurred while entering the base URI: ".concat(BASE_REQRES));
            Assertions.fail(exception);
        }

    }

    @When("the user wants to create their data")
    public void the_user_wants_to_create_their_data() {
        userData.setName(randomFirstName());
        userData.setJob(randomJob());

        try {
            USER.attemptsTo(
                    doPost()
                            .usingTheResource(PostResources.CREATE_USER_WITH_POST.getValue())
                            .setBodyRequest(userData.toJson())

            );
            LOGGER.info("Successful access to POST resource:".concat(PostResources.CREATE_USER_WITH_POST.getValue()));
            LOGGER.info("User data to modify:\n".concat(userData.toJson()));
        } catch (Exception exception) {
            LOGGER.error("An error occurred while getting POST resource:".concat(PostResources.CREATE_USER_WITH_POST.getValue()));
            Assertions.fail(exception);
        }

    }

    @Then("the user will see his information created")
    public void the_user_will_see_his_information_created() {
        checkHttpResponse().setExpectedResponse(HttpStatus.SC_CREATED).answeredBy(USER);
        createdData = createUser().answeredBy(USER);
        USER.should(
                seeThat("the name is the same that he entered",
                        validateResponse -> createdData.getName(), equalTo(userData.getName())),
                seeThat("the job is the same that he entered",
                        validateResponse -> createdData.getJob(), equalTo(userData.getJob())),
                seeThat("has a date",
                        validateResponse -> createdData.getName(), notNullValue())
        );

    }

}
